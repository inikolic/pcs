import numpy as np
import copy

class BeliefSystem:
    def __init__(self, numLayers):
        self.weights = np.random.dirichlet(np.ones(numLayers),size=1)[0]
        #np.ones function returns a new array of given shape and data type, where the element's value is set to 1
        # and dirichlet function is : https://numpy.org/doc/stable/reference/random/generated/numpy.random.dirichlet.html

    def get(self, layer):
        return self.weights[layer]

class BoundarySystem:
    def __init__(self, maxAvailableMargin):
        self.resourceMargin = np.random.uniform(0,maxAvailableMargin)

class DepartmentPolicy:
    def __init__(self):
        # Random initialisation of the Diagnostic Control System levers, as weight sets between 0 and 1

        ## Diagnostic Control System lever 1 ##
        # A pair of importance weights summing up to one
        # 1st weight represent a preference towards achieving short-term goals
        # 2nd weight represent a preference towards achieving long-term goals
        # Short-term goal: improvement compared to previous month (evaluation period)
        # Long-term goal: spend more energy on learning so that better results will come in the future (specified in lever 3)
        self.goalTermImportances = np.random.dirichlet(np.ones(2),size=1)[0]

        ## Diagnostic Control System lever 2 ##
        # Preference weights summing up to one
        # Represent preference towards the given types of knowledge acquiring
        # Learning is under the overall preference towards long-term goals
        # [0] social learning (sharing among people) 
        # [1] improving personal skills (specified in lever 3)
        # [2] exploring unknown edges (experimenting, without knowing the outcome)
        self.learningModePreferences = np.random.dirichlet(np.ones(3),size=1)[0]

        ## Diagnostic Control System lever 3 ##
        # Preference over the types of personal skills that the managers encourage employees to learn
        # [0] innovativeness
        # [1] prediction
        self.personalSkillPreferences = np.random.dirichlet(np.ones(2),size=1)[0]

        # Results
        self.rewards = []
        self.fitness_improvements = []
        self.organisation_fitness_improvements = []

    def getAverageReward(self):
        result = 0
        for r in self.rewards:
            result += r
        return result/len(self.rewards) if len(self.rewards) > 0 else 0

    def getAverageFitnessImprovement(self):
        result = 0
        for r in self.fitness_improvements:
            result += r
        return result/len(self.fitness_improvements) if len(self.fitness_improvements) > 0 else 0

    def getAverageOrganisationFitnessImprovement(self):
        result = 0
        for r in self.organisation_fitness_improvements:
            result += r
        return result/len(self.organisation_fitness_improvements) if len(self.organisation_fitness_improvements) > 0 else 0


class DepartmentPolicyChange:
    def __init__(self):
        self.goalTermImportances = [0,0]
        self.learningModePreferences = [0,0,0]
        self.personalSkillPreferences = [0,0]

class DiagnosticControlSystem:
    def __init__(self):
        self.policy = DepartmentPolicy()
        self.policyHistory = [self.policy]
        self.policyChangelog = [DepartmentPolicyChange()]

    def recordReward(self, reward):
        self.policy.rewards.append(reward)

    def recordFitnessImprovement(self, improvement):
        self.policy.fitness_improvements.append(improvement)

    def recordOrganisationFitnessImprovement(self, improvement):
        self.policy.organisation_fitness_improvements.append(improvement)

    def applyPolicyChange(self, change):
        new_policy = copy.deepcopy(self.policy)
        
        new_policy.goalTermImportances[0] += change.goalTermImportances[0]
        new_policy.goalTermImportances[1] += change.goalTermImportances[1]
        new_policy.learningModePreferences[0] += change.learningModePreferences[0]
        new_policy.learningModePreferences[1] += change.learningModePreferences[1]
        new_policy.learningModePreferences[2] += change.learningModePreferences[2]
        new_policy.personalSkillPreferences[0] += change.personalSkillPreferences[0]
        new_policy.personalSkillPreferences[1] += change.personalSkillPreferences[1]

        if new_policy.goalTermImportances[0] < 0:
            new_policy.goalTermImportances[0] == 0
        if new_policy.goalTermImportances[1] < 0:
            new_policy.goalTermImportances[1] == 0
        if new_policy.learningModePreferences[0] < 0:
            new_policy.learningModePreferences[0] == 0
        if new_policy.learningModePreferences[1] < 0:
            new_policy.learningModePreferences[1] == 0
        if new_policy.learningModePreferences[2] < 0:
            new_policy.learningModePreferences[2] == 0
        if new_policy.personalSkillPreferences[0] < 0:
            new_policy.personalSkillPreferences[0] == 0
        if new_policy.personalSkillPreferences[1] < 0:
            new_policy.personalSkillPreferences[1] == 0

        self.policyHistory.append(self.policy)
        self.policyChangelog.append(change)
        self.policy = new_policy
