import agentpy as ap
import networkx as nx
import matplotlib.pyplot as plt
from organisation.CEO import CEO
from landscape.multiplex import MultiplexLandscape


class SocialGraph:
    # No special overriding functionality necessary for the multilayer graph that is the social network,
    # but could be added here later
    def __init__(self):
        self.network = nx.Graph()

class PCSModel(ap.Model):
    def setup(self): # Model setup and agent creation
        self.landscape = self.fetchLandscape()
        self.organisations = list()
        self.socialGraph = SocialGraph().network  # Initialise the social graph as an empty graph
        self.CEOs = ap.AgentList(self, self.p.numCEOs, CEO) # Create CEO agents #self.p refers to model parameters as defined in main.py

        self.organisationRankingTimestep = 0 # When the organisation ranking was last requested

    def step(self): # Step/'go' of the model, i.e. which methods of which agents are performed per timestep
        self.debugFriendship = False # Was used for debugging Individual.socialise()
        self.everyoneSocialise()

        # Everyone's step function
        self.CEOs.managers.employees.step()
        self.CEOs.managers.step()
        self.CEOs.step()

        # self.verifySocialGraph()

    def fetchLandscape(self):
        return MultiplexLandscape(self.p.landscape)

    def verifySocialGraph(self):
        edge_colors = nx.get_edge_attributes(self.socialGraph, 'color').values()
        nx.draw_networkx(self.socialGraph, edge_color=edge_colors)

        if self.debugFriendship: self.printAgentsIdsAndTypes()
        plt.show()

    def everyoneSocialise(self):
        if self.t == 1: # Assumption: only at the beginning of the model run
            self.CEOs.socialise()
            self.CEOs.managers.socialise()
            self.CEOs.managers.employees.socialise()

    def requestOrganisationRanking(self): # Make a ranking of organisation's absolute performance, for CEOs to see which competitors outperform them
        if self.organisationRankingTimestep != self.t:
            self.organisationRankingTimestep = self.t
            organisation_list = self.organisations.copy()
            organisation_list.sort(key=lambda org: org.getAverageAbsoluteFitness(self.t - self.p.mcsEvaluationWindow, self.t), reverse=True)
            for i, org in enumerate(organisation_list):
                org.competitorsPerformingBetter = []
                if i > 0: # not the best performing company
                    org.competitorsPerformingBetter.append(organisation_list[i-1])
                    for j in range(2,org.competitorsToConsider+1):
                        if (i-j) > 0:
                            org.competitorsPerformingBetter.append(organisation_list[i-1])

    def update(self): # Used to record a dynamic variable. AgentPy standard; not used
        # Example: self.CEOs.record('my_variable)
        pass

    def end(self): # Used to report an evaluation measure upon the run ending. AgentPy standard; not used
        # Example: self.report('my_measure', 1)
        pass