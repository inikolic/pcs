import numpy as np

from control.mcs import BeliefSystem, BoundarySystem

class Organisation:
    def __init__(self, model, ceo):
        self.model = model
        self.ceo = ceo
        self.type = "org"
        self.id = self.type + str(self.ceo.id)  # We refer to organisations by the id of their CEO
        self.managers = list() # List < Manager >
        self.departments = list()  # List < Department >
        self.employees = list()  # List < Employee >
        self.pcsCounter = 0

        self.fitnessHistory = []
        self.absoluteFitnessHistory = []
        self.competitorsToConsider = self.model.random.randint(1, self.model.p.maxCompetitorsToConsider)
        self.competitorsPerformingBetter = []
        self.model.organisations.append(self)

        self.resources = (self.model.p.numManagersPerOrg + self.model.p.numEmployeesPerDept) * self.model.p.resourceMargin

        # Management Control System
        self.beliefSystem = BeliefSystem(self.model.landscape.num_layers)
        self.boundarySystem = BoundarySystem(self.model.p.maxResourceMargin)

    # TODO Decision cost is equaled to the number of edges. Can be extended in the future with this function.
    def decisionCostFunction(self, distance):
        return distance

    # TODO Decision reward function can be extended in the future with this function.
    def decisionRewardFunction(self, decisionCost, fitnessImprovement):
        return decisionCost * (1 + fitnessImprovement)

    # Get the organisational fitness (subjective; weighted with belief system)
    def getOrganisationalFitness(self):
        fitness = 0
        for employee in self.employees:
            fitness += employee.currentLocation.getIndividualFitness(self.beliefSystem.weights)
        fitness = fitness / len(self.employees) # Value between 0 and 1
        return fitness

    # Get the absolute fitness of the organisation (objective; not weighted with belief system)
    def getAbsoluteFitness(self):
        weights = [1 for i in range(self.model.landscape.num_layers)]
        fitness = 0
        for employee in self.employees:
            fitness += employee.currentLocation.getIndividualFitness(weights)
        fitness = fitness/len(self.employees)
        return fitness

    def getAverageAbsoluteFitness(self, startPeriod, endPeriod):
        result = 0
        for f in self.absoluteFitnessHistory[startPeriod:endPeriod]:
            result += f

        return result/(endPeriod - startPeriod)

    def step(self):
        pass