import numpy as np
import networkx as nx

import agentpy as ap
import math
import random
from organisation.Individual import Individual
from organisation.Employee import Employee
from organisation.Department import Department #why no error if initialization of department requires a manager ID? (vraag van Wouter)
from control.mcs import DiagnosticControlSystem, DepartmentPolicyChange
import copy

class ManagerReport:
    def __init__(self):
        self.proposals = []
    # Initializes an (empty) list of proposals

class Manager(Individual):
    def setup(self, organisationInitialLocation):

        # Pick an initial location close to the CEO
        potential_nodes = []
        for path_length in self.model.landscape.path_length:
            for node in self.model.landscape.layers[0].nodes:
                if path_length[organisationInitialLocation][node] >= (self.model.p.orgDepInitialDistance - 1) and \
                        path_length[organisationInitialLocation][node] <= (self.model.p.orgDepInitialDistance + 1):
                    if node not in potential_nodes:
                        potential_nodes.append(node)
        manager_initial_location = self.model.nprandom.choice(potential_nodes) if len(potential_nodes) > 0 else organisationInitialLocation
        # Similar to the function that creates an Employee's initial location close to the manager
        # CEO location = OrganisationInitionalLocation node 

        super().setup(manager_initial_location) # Perform agent-type "manager" setup in Individual class
        self.type = "manager"
    

        self.report = ManagerReport()
          # The code initializes the Managers' report object with an instance of the "ManagerReport" class.
        self.dcs = DiagnosticControlSystem()
           # The code initializes the Managers' dcs object of with an instance of the "DiagnosticControlSystem" class.
      


    def setupBookkeeping(self, ceo, organisation):
        self.ceo = ceo
        self.organisation = organisation
        self.department = Department(self.ceo, self.organisation, self)
         # A function to create and setup a bookkeeping method within the Manager class with parameters ceo, organisation.
        self.setupCreateEmployees()

    def setupCreateEmployees(self):
        # Create my employees
        self.numEmployees = self.p.numEmployeesPerDept #model parameter in main.py
        self.employees = ap.AgentList(self.model, self.numEmployees, Employee, managerInitialLocation=self.currentLocation.get())
        #TODO Not sure whether I understand this code correctly: The function creates an agentlist for the Employee class based on the number-ofemployees-per-department parameter
        # but what is the purpose of the 'get current location' based on manager's initial location since the Employee agent initialization has a setup function that initializes Employee location
        # or does it only pass this location to the agent (employee) so that the setup function can initiate a location derived from the manager location?

        # Add ties between the CEO and its managers to the social graph
        for employee in self.employees:
            employee.setupBookkeeping(self.ceo, self.organisation, self, self.department) # Pass own IDs to the employee

            self.model.socialGraph.add_edge(self, employee, type = "managerRelation", color = 'black')
            self.organisation.employees.append(employee)
            self.department.employees.append(employee)
        #TODO bovenstaande begrijp ik niet

    def employeeRewardFunction(self, employee, organisationImprovement):
        reward = employee.report.energyUsed # The minimum reward is the used energy
        weights = self.organisation.beliefSystem.weights

        # Calculate the different kinds of improvements of employees, and weigh them according to the different
        # aspects of the diagnostic control systems, to calculate a bonus energy reward
        actual_improvement = 0
        if employee.report.proposal.isApproved and len(employee.path) > 1: # Get improvement compared to employee's prevous location
            actual_improvement = employee.path[-1].getIndividualFitness(weights) - employee.path[-2].getIndividualFitness(weights)

        competence_improvement = employee.report.innovativenessIncrease * self.dcs.policy.personalSkillPreferences[0] + \
                                 employee.report.predictionCompetenceIncrease * self.dcs.policy.personalSkillPreferences[1]
        learning_improvement = (self.dcs.policy.learningModePreferences[0] * employee.report.newEdgesLearnt / employee.report.energyUsed \
            if employee.report.energyUsed > 0 else 0) + \
                               (self.dcs.policy.learningModePreferences[1] * competence_improvement) + (self.dcs.policy.learningModePreferences[2]*employee.report.experimentsConducted / employee.report.energyUsed \
                if employee.report.energyUsed > 0 else 0)

        multiplier = self.dcs.policy.goalTermImportances[0] * actual_improvement + self.dcs.policy.goalTermImportances[1] * learning_improvement
        if math.isnan(multiplier):
            multiplier = 0
        if multiplier > self.model.p.maxBonusMultiplier:
            multiplier = self.model.p.maxBonusMultiplier
        bonus = math.floor(multiplier * self.model.p.maxBonus) # Bonus energy allocated on top of the minimum reward
        reward = reward + bonus

        self.dcs.recordOrganisationFitnessImprovement(organisationImprovement)
        self.dcs.recordFitnessImprovement(actual_improvement)
        self.dcs.recordReward(reward)

        return reward

    def rewardEmployees(self, organisationImprovement): # Tell employees how much energy they are rewarded
        for employee in self.employees:
            reward = self.employeeRewardFunction(employee, organisationImprovement)
            employee.reimbursement += reward

    def reimburseEmployees(self): # Add rewarded energy to my own energy level
        for employee in self.employees:
            employee.totalEnergy += employee.reimbursement
            employee.reimbursement = 0

    def assessDiagnosticControlSystem(self):
        reward_improvement = self.dcs.policy.getAverageReward() - self.dcs.policyHistory[-1].getAverageReward()
        dep_fitness_improvement = self.dcs.policy.getAverageFitnessImprovement()
        org_fitness_improvement = self.dcs.policy.getAverageOrganisationFitnessImprovement()

        change = DepartmentPolicyChange()

        # If my last DCS change improved my fitness, then repeat it, hopefully improving the fitness further
        if reward_improvement > 0 and len(self.dcs.policyHistory) > 1:
            change = self.dcs.policyChangelog[-1]
        # If the last improvement did not work, then undo it, and try another random change for one of the three levers
        else:
            self.dcs.policy = copy.deepcopy(self.dcs.policyHistory[-1])
            change = DepartmentPolicyChange()
            item_to_update = self.model.random.randint(0, 6)
            if item_to_update == 0:
                change.goalTermImportances[0] = self.model.p.mcsGradientStep
                change.goalTermImportances[1] = -1 * self.model.p.mcsGradientStep
            if item_to_update == 1:
                change.goalTermImportances[0] = -1 * self.model.p.mcsGradientStep
                change.goalTermImportances[1] = self.model.p.mcsGradientStep
            if item_to_update == 2:
                change.learningModePreferences[0] = self.model.p.mcsGradientStep
                change.learningModePreferences[1] = (-1/2) * self.model.p.mcsGradientStep
                change.learningModePreferences[2] = (-1/2) * self.model.p.mcsGradientStep
            if item_to_update == 3:
                change.learningModePreferences[0] = (-1/2) * self.model.p.mcsGradientStep
                change.learningModePreferences[1] = self.model.p.mcsGradientStep
                change.learningModePreferences[2] = (-1/2) * self.model.p.mcsGradientStep
            if item_to_update == 4:
                change.learningModePreferences[0] = (-1/2) * self.model.p.mcsGradientStep
                change.learningModePreferences[1] = (-1/2) * self.model.p.mcsGradientStep
                change.learningModePreferences[2] = self.model.p.mcsGradientStep
            if item_to_update == 5:
                change.personalSkillPreferences[0] = self.model.p.mcsGradientStep
                change.personalSkillPreferences[1] = -1 * self.model.p.mcsGradientStep
            if item_to_update == 6:
                change.personalSkillPreferences[0] = -1 * self.model.p.mcsGradientStep
                change.personalSkillPreferences[1] = self.model.p.mcsGradientStep

        # If I underperform compared to my organisation's other departments, then increase short-term priority
        if dep_fitness_improvement < org_fitness_improvement:
            change.goalTermImportances[0] += self.model.p.mcsGradientStep
            change.goalTermImportances[1] += -1 * self.model.p.mcsGradientStep

        self.dcs.applyPolicyChange(change)

    def step(self):
        # Go over employee reports
        for employee in self.employees:
            # Forward proposals to the CEO
            self.report.proposals.append(employee.report.proposal)

        # Reimburse employees with regard to the reimbursement window
        if self.model.t % self.model.p.reimbursementWindow == 0:
            self.reimburseEmployees()

        # Evaluate the diagnostic control system once every mcsEvaluationWindow ticks
        if self.model.p.mcs:
            if self.model.t % self.model.p.mcsEvaluationWindow == 0:
                self.assessDiagnosticControlSystem()

        # Log statistics
        self.record('org_id', self.organisation.ceo.id)
        self.record('dcs_goal_term_importances', self.dcs.policy.goalTermImportances)
        self.record('dcs_learning_mode_preferences', self.dcs.policy.learningModePreferences)
        self.record('dcs_personal_skill_preferences', self.dcs.policy.personalSkillPreferences)
        self.record('dcs_average_reward', self.dcs.policy.getAverageReward())
        self.record('dcs_average_fitness_improvement', self.dcs.policy.getAverageFitnessImprovement())