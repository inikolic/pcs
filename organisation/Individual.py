import agentpy as ap
import numpy as np
from random import choice
import random
from itertools import combinations #not used?

from landscape.knowledge import Knowledge, union, hasOverlap

class IndividualLocationPerception:
    def __init__(self, location, num_layers):
        self.location = location
        self.estimatedFitness = ["unknown" for _ in range(num_layers)] # unknown yet
        self.realisedFitness = ["unknown" for _ in range(num_layers)] # unknown yet, will only know if the proposal is approved and the employee actually steps there
        # initialize three attributes that belong to each agent (self) and depends on 2 parameters: (1) their location and (2) the number of layers the agent can interpret:
        # (1) coordinates of a landscape location (not necessarily the agent's actual current location)
        # (2) the fitness they estimate for that location (relevant for locations where the agent considers to move to)
        # (3) the realised fitness value of a location where the agent actually is (not sure yet whether there is a knowledge/interpretation bias applicable)
        # if the fitness is unknown or undefined, the value -1 is provided

    def recordFitnessEstimation(self, layer, fitness):
        self.estimatedFitness[layer] = fitness
        # function that records the agent's estimated fitness value for each layer in the multiplex landscape

    def recordRealisedFitness(self, layer, fitness):
        self.realisedFitness[layer] = fitness
        # function that records the agent's realised fitness value for each layer in the multiplex landscape

    def get(self):
        return self.location
        # function that returns an agent's location coordinates

    def getIndividualFitness(self, weights):
        fitness_values = []
        individual_weights = []
        for l_idx in range(len(self.estimatedFitness)): #l_idx = loop index variable for the number of items (the length) in the tuple that contains estimated fitness values of an agent
            if self.estimatedFitness[l_idx] != "unknown" or self.realisedFitness[l_idx] != "unknown":
                if self.realisedFitness[l_idx] != "unknown":
                    fitness_values.append(self.realisedFitness[l_idx])
                else:
                    fitness_values.append(self.estimatedFitness[l_idx])
                individual_weights.append(weights[l_idx])

        # Rescale weights to sum to 1
        normalized_weights = [float(weight)/sum(individual_weights) for weight in individual_weights]

        # iterate though the values and weights
        result = 0
        for fitness, weight in zip(fitness_values, normalized_weights):
            result += fitness * weight
        return result
        # This function calculates the fitness of an agent based on the estimated fitness and realised fitness values of the agent and a set of weights.
        # The function takes two arguments: 'self', which refers to the current instance of the class that the function is defined in, and 'weights', which is a list of weights for each fitness value. 
        # The function first initializes a variable called 'result' to 0. It then iterates through the estimated fitness values of the agent using a for loop, using 'l_idx' as the loop index variable.
        # For each fitness value, the function checks if the realised fitness value of the agent at the same index is greater than -1. If it is, then the function uses the realised fitness value instead of the estimated fitness value for that index.
        # The function then multiplies the fitness value at that index by the corresponding weight from the 'weights' list and adds it to the 'result' variable.
        # Finally, the function returns the average value of 'result' by dividing it by the length of the 'weights' list.


class Memory:
    def __init__(self, size):
        self.locations = []
        self.size = size
        # initializes a class Memory with two agent attributes: (1) a new empty list and (2) the memory size 
    
    def remember(self, l):
        updated = False
        for i in range(len(self.locations)):
            if self.locations[i].get() == l.get():
                self.locations[i] = l
                updated = True
        # This function takes the argument 'l' to update the agent's memory.
        # First, the function initializes the boolean variable 'updated' to False
        # The loop index 'i' is then used to iterate over the indices of the agent's locations list. 
        # For each location in the locations list, the function checks if the get() method of the l object passed as an argument returns the same value as the get() method of the i-th location object in the locations list.
        # If the values returned by the get() methods match, the function updates the i-th location object in the locations list with the l object passed as an argument, and sets the updated variable to True.

        if not updated:
            self.locations.insert(0, l)
         # After the loop, if the updated variable is still False, the function adds the l object to the beginning of the locations list using the insert() method.
        
        if len(self.locations) > self.size:
            self.forget()
        # Finally, the function checks if the length of the locations list is greater than the size attribute of the class instance. If it is, the function calls a forget() method to remove the oldest location from the list.

    #Overall, this function is used to update the locations list with a new object passed as an argument, if a matching object is found in the list. 
    #If a match is found, the function replaces the old object with the new one. If no match is found, then the function inserts the new object at the beginning of the list. 
    #The function also ensures that the locations list does not exceed a certain size, by removing the oldest location from the list if it does, see next function forget(self)

    def forget(self):
        return self.locations.pop()
    # this function removes the last item in the self.locations list
    
    def isEmpty(self):
        return len(self.locations) == 0

    def oldest(self):
        return self.locations[-1]

    def latest(self):
        return self.locations[0]

    def search(self, location):
        results = [r for r in filter(lambda l: l.get() == location, self.locations)]
        if len(results) > 0:
            return results[0]
        else:
            return None

class Individual(ap.Agent):
    def setup(self, initialLocation):
        self.initPersonalCompetency()
        self.currentLocation = self.initLocation(initialLocation)
        self.path = [self.currentLocation]
        self.knowledge = self.initKnowledge(initialLocation)


        memorySize = self.model.random.randint(self.model.p.minMemory, self.model.p.maxMemory)
        self.memory = Memory(memorySize)
        self.memory.remember(self.currentLocation)

        self.deptColleaguesList = [] 
        self.orgColleaguesList = []
        self.friends = []
    # This is a class definition for an agent in the AgentPy library.
    # The class is called Individual and it inherits from ap.Agent, which is a base class in the AgentPy library for defining agents.
    # The setup method is defined within the class and it initializes various attributes of the agent, such as its personal competency, current location, path, and knowledge. The initPersonalCompetency, initLocation, and initKnowledge methods are presumably defined elsewhere in the code and are called to set the initial values for these attributes.
    # The code also randomly sets the size of the agent's memory and creates a new Memory object with that size, which is used to store the agent's past visited locations. The remember method of the Memory object is called to remember the agent's current location.
    # Lastly, the code initializes empty lists for the agent's departmental colleagues, organizational colleagues, and friends. These lists are presumably populated later in the code.

    def initPersonalCompetency(self):
        self.totalEnergy = self.model.random.randint(self.model.p.reimbursementWindow, self.model.p.reimbursementWindow*10)
        # Initial energy: have at least enough energy for a month (default setting reimbursementWindow parameter, see main.py) to propose a one-step move every day, and max 10 times that
        
        self.maxNumFriends = self.model.random.randint(0, self.model.p.maxMaxNumFriends)
        # sets randum number of maximum friends an agent can have (network size) between 0 and maxMaxNumFriends

        self.innovativeness = self.model.nprandom.triangular(0, self.model.p.modeInnovativeness, 1)
        self.predictionCompetence = 0 #temporarily disabled TODO turn back on: #self.model.nprandom.triangular(0, self.model.p.modePredictionCompetence, 1)
        # Personal competencies are random numbers with triangular distribution between 0 and 1, with an input parameter as mode
        # Note: use numpy.random instead of random.random, because random.random does not check for lowerBound < mode < upperBound
        # Note: triangular distribution is used since the outcome range has a minimum (0), a maximum (1) and a peak (most likely) parameter (e.g. self.model.p.modeInnovativeness; see main.py)
            # https://en.wikipedia.org/wiki/Three-point_estimation
    

    def initLocation(self, initialLocation):
        location = IndividualLocationPerception(initialLocation, self.model.landscape.num_layers)
        
        for l_idx in range(self.model.landscape.num_layers):
            location.recordRealisedFitness(l_idx, self.model.landscape.getFitnessForLayer(l_idx, initialLocation))

        return location
        # The initLocation function initializes an IndividualLocationPerception object based on the initialLocation parameter and the number of multiplex layers defined in the self.model.landscape.
        # The IndividualLocationPerception object stores the realized fitness values for the given location and layers. The function records the realized fitness value for each layer using the recordRealisedFitness method of the IndividualLocationPerception object.
        # Finally, the function returns the IndividualLocationPerception object representing the initial location.

    def initKnowledge(self, sourceNode):
        layers = self.model.random.choices([i for i in range(0, self.model.landscape.num_layers)], k=self.model.p.initKnowledgeLayerNum)
        return Knowledge(self.model.landscape, sourceNode, layers, self.model.p.initKnowledgeNodeNum, self.model.p.initKnowledgeGraphDegree, self.model.p.mindCapacity)
        # This function initializes the knowledge of an agent in a model. It takes a sourceNode as an input, which is the initial location of the agent. 
        # The function generates a list of layer numbers using the random.choices method, which selects k layer numbers from the range of 0 to the number of layers in the model's landscape. 
        # The function then creates a new Knowledge object using these parameters along with the number of initial knowledge nodes, initial knowledge graph degree, and mind capacity specified in the model parameters (see main.py). 
        # The Knowledge object contains information about the agent's knowledge, including the nodes and edges in their mental representation of the landscape.

    def getFriends(self):
        # Determine which neighbors are already their friends (as opposed to their boss or subordinate)
        self.neighborList = [n for n in
                             self.model.socialGraph.neighbors(self)]  # Yields only direct neighbors, i.e. 1 edge away
        friends = list()
        for neighbor in self.neighborList:
            if self.model.socialGraph.edges[(self, neighbor)]["type"] == "friendRelation":
                friends.append(neighbor)

        return friends
        # The function first creates a list of all the instance's neighbors (nodes that are directly connected to the instance) in the social graph using a list comprehension. 
        # It then iterates through this list, checking the type of the edge connecting the instance to each neighbor in the social graph. 
        # If the type of the edge is "friendRelation", meaning that the neighbor is a friend of the instance, it appends the neighbor to a list called "friends". 
        # Finally, the function returns the "friends" list.

    def getColleagues(self):
        deptColleaguesList = list() # Colleagues within my organisation and department
        orgColleaguesList = list() # Colleagues within my organisation, outside my department
        for node in self.model.socialGraph.nodes: # Evaluate all nodes (individuals)
            if node.organisation == self.organisation and node.type != "CEO": # Check if the organisations are the same

                # CEOs have no department; they thus only have orgColleagues
                if self.type == "CEO":
                    orgColleaguesList.append(node)
                    if self.model.debugFriendship: print('I am node', self, 'and I am orgcolleagues with', node)

                elif node.department == self.department: # If the departments also overlap, we are deptColleagues
                    deptColleaguesList.append(node)
                    if self.model.debugFriendship: print('I am node', self, 'and I am depcolleagues with', node)
                else:
                    orgColleaguesList.append(node)
                    if self.model.debugFriendship: print('I am node', self, 'and I am orgcolleagues with', node)
        return deptColleaguesList, orgColleaguesList

    def socialise(self):
        self.friends = self.getFriends()
        self.numFriends = len(self.friends)
        self.deptColleaguesList, self.orgColleaguesList = self.getColleagues()

        # If there is room for more friends, befriend someone
        while self.numFriends < self.maxNumFriends:
            eligible_nodes = set(self.model.socialGraph.nodes()) # Initially, all nodes are eligible to befriend. Later, we reduce this set

            # Find out which nodes are my colleagues; don't befriend them (except that employees can befriend the
            # CEO) Don't befriend people that are already your neighbor, i.e. current friends, or employees'
            # managers, or managers' CEOs or employees, or yourself Inspiration taken from
            # https://stackoverflow.com/questions/10929269/how-to-select-two-nodes-pairs-of-nodes-randomly-from-a-graph-that-are-not-conn

            # The direct neighbors and colleagues within the department won't be befriended; this is registered in
            # exclude_list. First make a list of which nodes these are, and then remove them from the set of eligible
            # nodes NOTE: the orgColleaguesList is not used here
            exclude_list = [*self.neighborList, *self.deptColleaguesList]
            exclude_list.append(self) # Might seem redundant, but is necessary to catch CEOs trying to befriend
            eligible_nodes.difference_update(exclude_list)

            # Out of the remaining eligible nodes, select a random person to befriend
            if len(eligible_nodes) > 0:
                new_friend = self.model.random.choice(list(eligible_nodes))
            else:
                break # If there are no eligible nodes to befriend, exit the while-loop

            # Become friends: add the edge in the social graph, and update my own friendlist
            self.model.socialGraph.add_edge(self, new_friend, type="friendRelation", color = "green")
            self.numFriends += 1 # Update number of friends for next iteration of loop
            self.friends.append(new_friend)
        # TODO Assumption: number of friends doesn't change throughout the run. Friendships are static, not dynamic.
        #  In reality, this may be different, but modelling friendships dynamically would overlap with the
        #  functionality of the PCS. We expect that adding dynamic friendships would lessen the difference between experiments with and without PCS
        #  On the other hand, it can be argued that new friends may arise from PCS, i.e. that PCS preceedes making new friends
        # TODO verify if relationships that have developed from PCS break after a PCS is dissolved, or not. 
        #       Could be relevant if we want to test whether PCSs lead to higher quality of internal networks and therefore more complexity and adaptivity going forward.