import numpy as np
import networkx as nx
import random
from random import choice
import math

from organisation.Individual import Individual, IndividualLocationPerception
from organisation.PCS import PCS
from landscape.knowledge import union
from landscape.knowledge import hasOverlap
from landscape.knowledge import getShareableKnowledge

class NodeLayer: # Only to easily access node-layer pairs
    def __init__(self, node, layer):
        self.node = node
        self.layer = layer

class Proposal:
    def __init__(self, employee):
        self.employee = employee
        self.source = None # 'Old' location (decision)
        self.target = None # Proposed new location (decision)
        self.improvement = 0
        self.cost = 0
        self.isApproved = False
        self.counter = 0 #toegevoegd Wouter
        self.approvalCounter = 0 #toegevoegd Wouter

class EmployeeReport:
    def __init__(self, employee):
        self.employee = employee
        self.proposal = Proposal(employee)
        self.energyUsed = 0
        self.newEdgesLearnt = 0
        self.experimentsConducted = 0
        self.innovativenessIncrease = 0
        self.predictionCompetenceIncrease = 0

    def clear(self):
        self.energyUsed = 0
        self.newEdgesLearnt = 0
        self.experimentsConducted = 0
        self.innovativenessIncrease = 0
        self.predictionCompetenceIncrease = 0

class Employee(Individual):
    def setup(self, managerInitialLocation):
      
        # Pick an initial location close to the manager
        potential_nodes = []
        for path_length in self.model.landscape.path_length:
            for node in self.model.landscape.layers[0].nodes:
                if path_length[managerInitialLocation][node] >= (self.model.p.depEmployeeInitialDistance - 1) and \
                        path_length[managerInitialLocation][node] <= (self.model.p.depEmployeeInitialDistance + 1):
                    if node not in potential_nodes:
                        potential_nodes.append(node)
        employee_initial_location = self.model.nprandom.choice(potential_nodes) if len(potential_nodes) > 0 else managerInitialLocation

        super().setup(employee_initial_location) # Perform setup in Individual class
        self.type = "employee"

        self.reimbursement = 0
        self.report = EmployeeReport(self)
        self.proposalHistory = []
        self.proposalAveragingWindow = self.model.p.proposalAveragingWindow
        self.ownedPCSes = list()  # PCSes I am the owner of
        self.PCSes = list() # PCSes I am a member of
        self.PCSInvitations = list()
        #self.ERIThreshold = self.model.p.ERIThreshold # The Expected Relative Improvement (ERI)-threshold
        self.bonusThreshold = self.model.p.bonusThreshold # Threshold that defines which bonus level satisfices the employee

    def setupBookkeeping(self, ceo, organisation, manager, department):
        self.ceo = ceo
        self.organisation = organisation
        self.manager = manager
        self.department = department
      
    def exploreLandscape(self, availableEnergyForExploration, availableEnergyForExperimenting):
        energy_left = availableEnergyForExploration
        experiments_left = availableEnergyForExperimenting

        knowledge = self.knowledge
        innovativeness = self.innovativeness
        predictionCompetence = self.predictionCompetence
        if self.model.p.pcs: # If PCSes are enabled for this model run
            if len(self.PCSes) > 0: # If I am in a PCS, then use the aggregated knowledge and competences instead of my own
                pcs = self.PCSes[0] #TODO We assume one can only be in 1 PCS at a time, but self.PCSes is a list to allow extension
                knowledge = pcs.assembledKnowledge
                innovativeness = pcs.combinedInnovativeness
                predictionCompetence = pcs.combinedPredictionCompetence

        # Generating weights according to the Belief System and the individual knowledge
        weights = []
        for l_idx in range(self.model.landscape.num_layers):
            if knowledge.isLayerActive[l_idx]:
                weights.append(self.organisation.beliefSystem.get(l_idx))
            else:
                weights.append(0)

        # Track the locations already visited in this exploration round
        locations_visited = [self.currentLocation]
        potential_locations_to_revisit = [self.currentLocation.get()]

        # While one has energy, keep exploring
        while energy_left > 0:
            if len(potential_locations_to_revisit) == 0: # avoid infinite loop
                break

            source_node = potential_locations_to_revisit[0] # By default, start looking from the current location
            if self.model.random.random() < innovativeness:
                # If one is innovative enough, select a node beyond the default starting node
                source_node = potential_locations_to_revisit[-1]

            potential_nodes = []
            for layer in knowledge.layers:
                # Find neighbours from the location one is currently exploring
                neighbors = layer.neighbors(source_node)
                for node in neighbors:
                    if node not in potential_nodes:
                        potential_nodes.append(node)

            if len(potential_nodes) > 0:
                # Do not consider nodes already visited in this round
                potential_new_nodes = [node for node in potential_nodes if node not in map(lambda l: l.get(), locations_visited)]
                if len(potential_new_nodes) > 0:
                    chosen_node = self.model.nprandom.choice(potential_new_nodes)

                    # Check whether one has already visited this node and still remembers. If not, make a prediction on its fitness value
                    memory_result = self.memory.search(chosen_node)
                    if memory_result == None: #
                        # The prediction is biased by an error depending on personal competencies; this error can be both positive and negative
                        fitness_list = self.model.landscape.getLandscapeFitness(chosen_node)
                        prediction_error_abs = 0 #self.model.nprandom.uniform(0, 1 - predictionCompetence)
                        prediction_error = prediction_error_abs if self.model.random.random() < 0.5 else -1 * prediction_error_abs
                        # Make sure each estimated fitness is between 0 and 1
                        estimated_fitness_list = [max(0, min(1, f + prediction_error)) for f in fitness_list]

                        # Add new memory of estimation
                        new_location_memory = IndividualLocationPerception(chosen_node, self.model.landscape.num_layers)
                        for l_idx in range(len(knowledge.layers)):
                            if knowledge.isLayerActive[l_idx]:
                                new_location_memory.recordFitnessEstimation(l_idx, estimated_fitness_list[l_idx])

                        self.memory.remember(new_location_memory)
                        locations_visited.append(new_location_memory)
                        potential_locations_to_revisit.append(new_location_memory.get())
                        energy_left -= 1
                    else: # If already visited, recall the fitness that is in my memory
                        locations_visited.append(memory_result)
                        potential_locations_to_revisit.append(memory_result.get())
                else: # No more potential new nodes to explore from this source node
                    potential_locations_to_revisit.remove(source_node)
            else: # No more potential new nodes to explore from this source node
                potential_locations_to_revisit.remove(source_node)

            if energy_left > 0:
                energy_left -= 1

        # Propose the best move from the ones explored in this round
        proposed_move = max(locations_visited, key=lambda l: l.getIndividualFitness(weights))
        energy_used = availableEnergyForExploration - energy_left

        expected_improvement = proposed_move.getIndividualFitness(weights) - self.currentLocation.getIndividualFitness(weights)
        expected_bonus_level = self.manager.dcs.policy.goalTermImportances[0] * expected_improvement * 10 #TODO parametriseren

        # Experimentation phase:
        # If I am not in a PCS, but am desperate for improvements because my ERIThreshold is not met, then start experimentation
        if len(self.PCSes) == 0:
            #?if expected_improvement / self.currentLocation.getIndividualFitness(weights) < self.ERIThreshold: # Expected relative improvement < threshold
            if expected_bonus_level < self.bonusThreshold:
                experiment_locations = [proposed_move] # By default, start looking from the last proposed location
                while experiments_left > 0:
                    source_node = experiment_locations[0].get()
                    if self.model.random.random() < self.innovativeness:
                        # If one is innovative enough, select a node beyond the default starting node
                        source_node = experiment_locations[-1].get()

                    # Find neighbours from the location one is currently exploring
                    # Assumption: cannot learn about edges in unknown layers. Layers can only be discovered when learnt from someone else
                    potential_nodes = []
                    for l_idx, layer in enumerate(self.knowledge.layers):
                        neighbors = self.knowledge.unknownNeighbors(source_node, l_idx)
                        for node in neighbors:
                            if node not in potential_nodes:
                                potential_nodes.append(NodeLayer(node, l_idx))

                    if len(potential_nodes) > 0:
                        chosen_node = self.model.nprandom.choice(potential_nodes) # Select a random node
                        node_experiment = IndividualLocationPerception(chosen_node.node, self.model.landscape.num_layers)
                        # Because I lack knowledge about this node, assume the fitness is equal (so not worse, nor better) to that of my prior 'best' proposal
                        node_experiment.estimatedFitness = proposed_move.estimatedFitness
                        experiment_locations.append(node_experiment)
                        self.knowledge.addEdge((source_node, chosen_node.node,), chosen_node.layer) # Learn about the node's existence, hence add an edge hereto in my knowledge graph

                    experiments_left -= 1

                # Update the proposed node to the one of the last experiment. In reality, any experimented location
                # can be chosen, as the fitness is assumed to be equal to the last best proposal either way
                proposed_move = experiment_locations[-1]
                self.report.experimentsConducted = len(experiment_locations[1:])

        # Add the proposed move to my report
        distance = knowledge.distance(self.currentLocation.get(), proposed_move.get())
        cost_of_move = self.organisation.decisionCostFunction(distance)
        self.report.proposal.isApproved = False
        self.report.proposal.counter += 1
        self.report.energyUsed = energy_used
        self.report.proposal.source = self.currentLocation
        self.report.proposal.target = proposed_move
        self.report.proposal.improvement = expected_improvement
        self.report.proposal.cost = cost_of_move
        self.proposalHistory.append(self.report.proposal)
        self.totalEnergy -= energy_used


    #?def assessExpectedPerformance(self):
        # Take the current expected improvement and the average of the previous proposalAveragingWindow improvements
        #?current_expected_improvement = 0
        #?average_improvement = 0
        #?for i in range(-1, -1*(self.proposalAveragingWindow+2), -1):
            #?if i == -1:
                #?current_expected_improvement = 1 + self.proposalHistory[i].improvement
            #?else:
                #?average_improvement += self.proposalHistory[i].improvement

        #?average_improvement = 1 + average_improvement / (self.proposalAveragingWindow-1)

        # Expected relative importance: the fraction of the currently expected improvement relative to the previously
        # average improvement
        #?eri = current_expected_improvement/average_improvement
        #?return eri

    def assessExpectedBonus(self):
        expected_bonus_level = self.proposalHistory[-1].improvement * self.manager.dcs.policy.goalTermImportances[0] * 10
        return expected_bonus_level

    def participateInPCSes(self, PCSToEvaluate):
        PCSToEvaluate.checkAge()

        # When the PCS' step function is triggered, it could disband, hence check if it's alive before proceeding
        if PCSToEvaluate.alive:
            # If I own a PCS, but am the only member thereof (which both follows from the PCS having only 1 member,
            # i.e. me), and I received an invitation for another PCS, then abandon mine and join the other. If not,
            # invite others
            if len(PCSToEvaluate.members) == 1 and len(self.PCSInvitations) > 0:
                PCSToEvaluate.disband()  # Disband the PCS I was the lonely member of
                # Note: joining the other PCS will happen further down in the employee's step-function, so is not
                # listed here yet.
            else:
            # If I'm the owner of a PCS, but the PCS is not effective enough (i.e. proposal is not that good), ask
            # the manager to extend the PCS. But note: can only assess the ERI if enough historical data is available
                if PCSToEvaluate.owner == self: #? and len(self.proposalHistory) > self.proposalAveragingWindow:
                    expected_bonus_level = self.assessExpectedBonus()
                    if expected_bonus_level < self.bonusThreshold:
                        PCSToEvaluate.inviteNewParticipants(self.manager)

    def actOnPCSInvitations(self): # Act on pending PCS invitations
        # TODO: Possibility to add other conditions to joining a PCS, representing willingness or energy. For
        #  example: joining a PCS could be made to cost energy or financial resources. Or: even if the user wants to
        #  join a PCS, there is a certain probability that it does not happen. Or: even if the user does not want to
        #  join a PCS because of a subpar ERI, there is still a probability that it does join one

        self.updatePCSInvitations() # Remove disbanded PCSes from my invitations
        if len(self.PCSInvitations) > 0:
            PCSToJoin = self.model.nprandom.choice(self.PCSInvitations) # Pick a random PCS to join
            PCSToJoin.addMember(self) # Add myself as a member to the PCS' list of members
            self.PCSes.append(PCSToJoin) # Add the PCS to the list of PCSes I participate in

    def initiatePCS(self):
        initiatedPCS = PCS(self) # Construct a new PCS. Pass a reference to myself to the constructor, to serve as owner
        if initiatedPCS.initialInvitationCount == 0: # If no others can be invited to the PCS, I'll remain its only member, so delete it
            del initiatedPCS
            self.organisation.pcsCounter -= 1
        else:
            self.ownedPCSes.append(initiatedPCS) # Append the PCS to the list of PCSes that I own and participate in
            self.PCSes.append(initiatedPCS)

    def developCompetencies(self, availableEnergy):
        # Determine available energy for competency development based on DCS
        energy_for_improving_innovativeness = math.floor(availableEnergy * self.manager.dcs.policy.personalSkillPreferences[0])
        energy_for_improving_prediction = math.floor(availableEnergy * self.manager.dcs.policy.personalSkillPreferences[1])
        
        self.innovativeness += energy_for_improving_innovativeness * self.model.p.competencyLearningStep
        self.predictionCompetence += energy_for_improving_prediction * self.model.p.competencyLearningStep

        # Competencies are values between 0 and 1
        if self.innovativeness > 1:
            self.innovativeness = 1
        if self.predictionCompetence > 1:
            self.predictionCompetence = 1

        self.report.innovativenessIncrease = energy_for_improving_innovativeness*self.model.p.competencyLearningStep
        self.report.predictionCompetenceIncrease = energy_for_improving_prediction*self.model.p.competencyLearningStep

    def allocateEnergy(self): # Allocate energy based on DCS
        energy_divider = self.model.p.reimbursementWindow % self.model.t
        if energy_divider == 0:
            energy_divider = self.model.p.reimbursementWindow
        step_energy = self.totalEnergy / energy_divider # Energy to spend in this timestep
        energy_to_explore = math.ceil(step_energy*self.manager.dcs.policy.goalTermImportances[0])
        energy_for_social_learning = math.ceil(step_energy * self.manager.dcs.policy.goalTermImportances[1] * self.manager.dcs.policy.learningModePreferences[0])
        energy_for_competency_development = math.ceil(step_energy * self.manager.dcs.policy.goalTermImportances[1] * self.manager.dcs.policy.learningModePreferences[1])
        energy_for_experimenting = math.ceil(step_energy * self.manager.dcs.policy.goalTermImportances[1] * self.manager.dcs.policy.learningModePreferences[2])

        return (energy_to_explore, energy_for_social_learning, energy_for_competency_development, energy_for_experimenting,)

    def receiveProposalApproval(self):
        weights = []
        for l_idx in range(self.model.landscape.num_layers):
            if self.knowledge.isLayerActive[l_idx]:
                weights.append(1)
            else:
                weights.append(0)

        # Update location to proposed location
        self.currentLocation = self.report.proposal.target
        self.path.append(self.currentLocation)
        self.report.proposal.isApproved = True
        self.report.proposal.approvalCounter += 1

        # Obtain the realised fitness and record it
        realised_fitness_list = self.model.landscape.getLandscapeFitness(self.currentLocation.get())
        for l_idx in range(len(self.knowledge.layers)):
            if self.knowledge.isLayerActive[l_idx]:
                self.currentLocation.recordRealisedFitness(l_idx, realised_fitness_list[l_idx])
                self.proposalHistory[-1].target.recordRealisedFitness(l_idx, realised_fitness_list[l_idx])
                self.memory.remember(self.currentLocation)

    def learningCostFunction(self):
        return self.model.p.learningCost
        #TODO Learning cost is a fixed model parameter. Can be extended in the future with this function.

    def learn(self, availableEnergy):
        learning_cost = self.learningCostFunction()
        available_energy = availableEnergy
        num_edges_learnt = 0
        if available_energy > learning_cost:
            # If the model were to be extended such that people can be in >1 PCS at a time, then some PCS-peers can have a higher chance of being 'teacher' than others. This seems reasonable as you encounter them more often, so chance to select them goes up

            # Get PCS peers
            pcs_peers = []
            if self.model.p.pcs:  # If PCSes are enabled for this model run
                if len(self.PCSes) > 0: # If I am in a PCS, then find out who my PCS peers are
                    for PCS in self.PCSes:
                        pcs_peers = pcs_peers + PCS.members
                    pcs_peers.remove(self) # Cannot learn from yourself

            friends_and_pcs_peers = [*self.friends, *pcs_peers]
            potential_teachers_to_select = self.model.p.learningAttempts
            if len(friends_and_pcs_peers) > 0:

                # Reduce the number of potential teachers to select if I don't have enough colleagues/friends to meet this upper bound
                if len(friends_and_pcs_peers) < potential_teachers_to_select:
                    potential_teachers_to_select = len(friends_and_pcs_peers)

                # TODO For code optimisation reasons (testing knowledge union and is_connected is very
                #  computationally intensive), we limit the number of teachers we attempt to learn from; configurable
                #  through learningAttempts. Randomly select potential teachers from the list of eligible potential
                #  teachers
                potential_teachers = self.model.random.sample(friends_and_pcs_peers, potential_teachers_to_select)

                # Test which PCS colleagues and friends I have a knowledge overlap with; I can learn from them
                for potential_teacher in potential_teachers:
                    if available_energy > learning_cost:
                        knowledge_list_to_test = [self.knowledge, potential_teacher.knowledge]  # List of the knowledge object of the learning agent and of the colleague or friend
                        is_connected = hasOverlap(knowledge_list_to_test)  # Yields True if the knowledge objects are a connected component, and False if not
                        if is_connected:  # Only if the two separate knowledge graphs turn out to be connected, the invitee can be invietd
                            # Start learning
                            shareable_edges = getShareableKnowledge(self.knowledge, potential_teacher.knowledge, False)
                            if len(shareable_edges) > 0: # If the knowledge graphs fully overlap, then nothing is learnt
                                edge_and_layer_to_learn = self.model.random.choice(shareable_edges) # Select one random edge that will be learnt; format is [(node start, node end), layer]
                                edge_to_learn = edge_and_layer_to_learn[0]
                                layer_to_learn = edge_and_layer_to_learn[1]
                                self.knowledge.addEdge(edge_to_learn, layer_to_learn) # Learn: add the edge to my knowledge graph in the appropriate layer
                                num_edges_learnt += 1
                                available_energy -= learning_cost  # Subtract learning cost from the available energy.
                                # Assumption: if a teacher is selected, but no edge is learnt, this does not cost the
                                # learning employee any energy
        energy_used = availableEnergy - available_energy
        self.totalEnergy -= energy_used
        return num_edges_learnt

    def updatePCSInvitations(self):
        # Remove disbanded PCSes from my invitations
        self.PCSInvitations[:] = [PCS for PCS in self.PCSInvitations if PCS.alive]

    def step(self):
        self.report.clear()
        initiatedPCS = None # no PCS initiated yet this timestep

        self.socialise() # Make friends

        # Allocate energy
        energy_to_explore, energy_for_social_learning, energy_for_competency_development, energy_for_experimenting = self.allocateEnergy()
        
        # Develop personal competencies
        self.developCompetencies(energy_for_competency_development)

        # Explore and propose
        self.exploreLandscape(energy_to_explore, energy_for_experimenting)

        if self.model.p.pcs:  # If PCSes are enabled for this model run
            in_PCS = False
            self.updatePCSInvitations() # Remove disbandedPCSes from my invitations
            if len(self.PCSes) > 0:  # If I participate in or own at least one PCS
                for PCS_to_evaluate in self.PCSes:
                    self.participateInPCSes(PCS_to_evaluate)
                    # When the PCS' step function is triggered upon participation, it could disband,
                    # hence check if it's alive before updating is_alive
                    if PCS_to_evaluate.alive:
                        in_PCS = True

            # If the employee is not currently in a PCS, and the Expected Relative Improvement is too low,
            # consider initiating or joining one
            if not in_PCS:
                # Only makes sense to assess if enough historical data is available
                #?if len(self.proposalHistory) > self.proposalAveragingWindow:
                    #?expected_relative_improvement = self.assessExpectedPerformance()
                    #?if expected_relative_improvement < self.ERIThreshold:
                        # Check whether I have pending invitations for a PCS that I can act on. If not, initiate one myself
                    expected_bonus_level = self.assessExpectedBonus()
                    if expected_bonus_level < self.bonusThreshold:
                        if len(self.PCSInvitations) == 0:
                            self.initiatePCS()
            
                            
                    self.actOnPCSInvitations()

        self.report.newEdgesLearnt = self.learn(energy_for_social_learning) # Learn new edges

        # Log statistics
        self.record('org_id', self.organisation.ceo.id)
        self.record('current_location', self.currentLocation.location)
        #self.record('proposals_generated', self.report.proposal.counter)
        #self.record('proposals_approved', self.report.proposal.approvalCounter)
        self.record('proposed_location', self.report.proposal.target)
        self.record('exp_delta_fitness_', self.report.proposal.improvement)
        
        weights_t = [1 for i in range(self.model.landscape.num_layers)]
        self.record('abs_fitness', self.currentLocation.getIndividualFitness(weights_t))
        self.record('exp_bonus', self.assessExpectedBonus())

        pcs_logs = []
        for pcs in self.ownedPCSes:
            if len(pcs.members) > 1:
                pcs_logs.append(pcs.log())
        self.record('pcs_logs', pcs_logs)