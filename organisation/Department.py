class Department:
    def __init__(self, ceo, organisation, manager):
        self.ceo = ceo
        self.organisation = organisation
        self.manager = manager
        self.employees = list()
        self.type = "dep"
        self.id = self.type + str(self.manager.id)
    # This function initializes a class Department which has 3 arguments (ceo, organisation and manager)
    # It initializes a list of employees as a variable and "dep" as an agent-type
    # The department id is a string composed of "dep" + the respective manager ID
