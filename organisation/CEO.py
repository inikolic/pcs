import numpy as np
import agentpy as ap
from operator import sub
import copy

from organisation.Individual import Individual
from organisation.Manager import Manager
from organisation.Organisation import Organisation


class CEO(Individual):
    def setup(self):
        initialLocation = self.model.nprandom.choice(self.model.landscape.layers[0].nodes) # Pick a random initial location
        super().setup(initialLocation) # Perform setup in Individual class
        self.type = "CEO"

        self.organisation = Organisation(self.model, self) # Create organisation object

        self.numManagers = self.p.numManagersPerOrg
        self.managers = ap.AgentList(self.model, self.numManagers, Manager, organisationInitialLocation=initialLocation) # Create manager agents

        # Add ties between the CEO and its managers to the social graph
        for manager in self.managers:
            manager.setupBookkeeping(self, self.organisation) # Pass own IDs to the manager

            self.model.socialGraph.add_edge(self, manager, type = "CEORelation", color = 'red')
            self.organisation.managers.append(manager)

    def assessBoundarySystem(self):
        # Calculates a weighted average gradient towards the better performing competitors' boundary systems
        # Changes the boundary system towards the resulting gradient, by a pre-determined step defined as a model parameter
        if len(self.organisation.competitorsPerformingBetter) > 0:
            weighted_margin_difference = 0
            denominator = 0
            for i, competitor in enumerate(self.organisation.competitorsPerformingBetter):
                weighted_margin_difference += (i+1) * (competitor.boundarySystem.resourceMargin - self.organisation.boundarySystem.resourceMargin)
                denominator += i+1
            weighted_margin_difference = weighted_margin_difference/denominator

            # Approach the calculated 'ideal' boundary system
            self.organisation.boundarySystem.resourceMargin += self.model.p.mcsGradientStep * weighted_margin_difference

    def assessBeliefSystem(self):
        # Calculates a weighted average gradient towards the better performing competitors' belief systems
        # Changes the belief system towards the resulting gradient, by a pre-determined step defined as a model parameter
        if len(self.organisation.competitorsPerformingBetter) > 0:
            gradients = []
            gradient_weighted_average = []
            for competitor in self.organisation.competitorsPerformingBetter:
                gradients.append(list(map(sub, competitor.beliefSystem.weights, self.organisation.beliefSystem.weights)))

            denominator = 0
            for i in range(len(gradients[0])):
                c = 0
                for j in range(len(self.organisation.competitorsPerformingBetter)):
                    c += (j+1) * gradients[j][i]
                    denominator += j + 1
                gradient_weighted_average.append(c)
            
            gradient_weighted_average = [x/denominator for x in gradient_weighted_average]

            # Approach the calculated 'ideal' belief system
            for i, w in enumerate(self.organisation.beliefSystem.weights):
                self.organisation.beliefSystem.weights[i] = w + gradient_weighted_average[i] * self.model.p.mcsGradientStep

       
    def assessProposals(self):
        proposals = []

        # Go over manager reports
        for manager in self.managers:
            for p in manager.report.proposals:
                proposals.append(p)

            manager.report.proposals = [] # Clearing manager report for the next round

        # Sort the proposal based on the improvements they bring about
        proposals.sort(key=lambda p: p.improvement, reverse=True)

        decisionCost = 0
        currentAbsoluteFitness = self.organisation.getAbsoluteFitness() # Get currently realised fitness. Absolute, because it already happened; it's not predicting
        resourceMargin = self.organisation.resources * (1 - self.organisation.boundarySystem.resourceMargin)

        for p in proposals:
            if resourceMargin > p.cost: # Implement all proposals that fit the allocated budget
                p.employee.receiveProposalApproval()
                self.organisation.resources -= p.cost
                resourceMargin -= p.cost
                decisionCost += p.cost

        # Log realised fitness, improvement, and so on
        newAbsoluteFitness = self.organisation.getAbsoluteFitness()
        self.organisation.absoluteFitnessHistory.append(newAbsoluteFitness)
        self.organisation.fitnessHistory.append(self.organisation.getOrganisationalFitness())
        improvement = newAbsoluteFitness - currentAbsoluteFitness

        # Calculate the reward based on the improvement
        reward = self.organisation.decisionRewardFunction(decisionCost, improvement)
        self.organisation.resources += reward
        for manager in self.managers:
            manager.rewardEmployees(improvement)

    def assessMCS(self):
        # Check for outperforming competitors
        self.model.requestOrganisationRanking()

        # Assess belief system
        self.assessBeliefSystem()
       
        # Assess boundary system
        self.assessBoundarySystem()
   
    def step(self):
        self.assessProposals()

        # Evaluate the management control systems once every mcsEvaluationWindow ticks
        if self.model.p.mcs:
            if self.model.t % self.model.p.mcsEvaluationWindow == 0:
                self.assessMCS()

        # Log statistics
        energy_list = list(map(lambda e: e.totalEnergy, self.organisation.employees))
        innovativeness_list = list(map(lambda e: e.innovativeness, self.organisation.employees))
        pc_list = list(map(lambda e: e.predictionCompetence, self.organisation.employees))
        self.record('fitness', self.organisation.getOrganisationalFitness())
        self.record('abs_fitness', self.organisation.getAbsoluteFitness())
        self.record('resources', self.organisation.resources)
        self.record('avg_energy', sum(energy_list)/len(energy_list))
        self.record('avg_innovativeness', sum(innovativeness_list)/len(innovativeness_list))
        self.record('avg_prediction_competence', sum(pc_list)/len(pc_list))
        self.record('belief_system', copy.deepcopy(self.organisation.beliefSystem).weights)
        self.record('boundary_system_resource_margin', self.organisation.boundarySystem.resourceMargin)
        self.record('num_pcs', self.organisation.pcsCounter)

        # Call the step function for each PCS in the company fter everything else had happened in the company
        for manager in self.managers:
            for employee in manager.employees:
                for pcs in employee.ownedPCSes:
                    pcs.step()