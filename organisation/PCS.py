import numpy as np
import random
from landscape.knowledge import union, hasOverlap, Knowledge     

class PCS:
    def __init__(self, owner):
        self.id = '{0}_{1}'.format(owner.id, owner.model.t)
        self.participants = []
        self.owner = owner
        self.managerOfOwner = self.owner.manager

        self.lifetime = self.owner.model.random.randint(1, self.owner.model.p.maxLifetime) # The PCS 'lives' for a random amount of timesteps, between 1 and a the model parameter maxLifetime
        self.dateOfBirth = owner.model.t # Date of birth is the current timestep of the model. Since PCS is no agentpy agent object, access this through the owner

        self.members = [owner] # Keep track of members in a list, with the owner as first entry
        self.desiredNumMembers = self.owner.model.random.randint(1, self.owner.model.p.maxPCSSize)

        # Initially, the aggregated knowledge and competencies are those of the PCS owner/initiator
        self.assembledKnowledge = Knowledge(owner.knowledge)
        self.combinedInnovativeness = owner.innovativeness
        self.combinedPredictionCompetence = owner.predictionCompetence

        self.managerOverrideUsed = False
        self.proposedMove = None
        self.alive = True
        self.owner.organisation.pcsCounter += 1
        self.initialInvitationCount = 0
        self.inviteNewParticipants(self.owner)

    def addMember(self, member):
        self.members.append(member)

    def inviteNewParticipants(self, inviter):
        invitation_count = 0
        num_potential_members = len(self.members) # Initialisation; used later
        num_spots_for_new_members = self.desiredNumMembers - num_potential_members

        # If the PCS is already at its max desired capacity, but the manager is asked to extend the PCS,
        # then we override the maximum capacity to add 1 additional member. This can be done one time at most
        manager_override_capacity = False
        if inviter == self.managerOfOwner and num_spots_for_new_members <= 0 and not self.managerOverrideUsed:
            manager_override_capacity = True
            self.managerOverrideUsed = True
            num_spots_for_new_members = 1

        if num_potential_members < self.desiredNumMembers or manager_override_capacity:
            eligible_invitees = []
            eligible_invitees_all_types = []
            if inviter == self.managerOfOwner:
                eligible_invitees_all_types = [*self.managerOfOwner.deptColleaguesList, *self.managerOfOwner.orgColleaguesList, *self.managerOfOwner.friends]

                # Only employees can contribute to a PCS. Managers can invite people, but don't participate in exploring, so cannot be invited
                for eligible_invitee in eligible_invitees_all_types:
                    if eligible_invitee.type == 'employee':
                        eligible_invitees.append(eligible_invitee)

            elif inviter == self.owner:
                # Can invite everyone that you have a social connection to
                #TODO If desired, the eligible set of people to join a PCS could be altered. E.g., not inviting department colleagues
                eligible_invitees_all_types = [*self.owner.deptColleaguesList, *self.owner.orgColleaguesList, *self.owner.friends]

                # Only employees can contribute to a PCS. Managers can invite people, but don't participate in exploring, so cannot be invited
                for eligible_invitee in eligible_invitees_all_types:
                    if eligible_invitee.type == 'employee':
                        eligible_invitees.append(eligible_invitee)

            # For every potential invitee, we check whether the invitee and the PCS owner share at least 1 node.
            # This means they have a common understanding / boundary object, and hence can communicate with each other.
            if len(eligible_invitees) > 0:
                selected_invitees = eligible_invitees

                for eligible_invitee in selected_invitees:
                    if eligible_invitee != inviter and eligible_invitee not in self.members: # Cannot invite current members nor the owner to join the PCS
                        knowledge_list_to_test = [self.owner.knowledge, eligible_invitee.knowledge] # List of the knowledge object of the owner and of the invitee
                        is_connected = hasOverlap(knowledge_list_to_test) # Yields True if the knowledge graphs share at least one node, and False if not
                        if is_connected: # Only if the two separate knowledge graphs turn out to be connected, the invitee can be invietd
                            eligible_invitee.PCSInvitations.append(
                                self)  # Add the PCS to the list of invitations received by the invitee
                            num_potential_members += 1
                            invitation_count += 1
                            if manager_override_capacity:
                                manager_override_capacity = False # This override can only be used once, i.e. to add 1 extra person
                                break
                            if invitation_count >= num_spots_for_new_members: # If no more room for new members, we don't have to evaluate the other selected_invitees anymore
                                break

        self.initialInvitationCount = invitation_count

    def disband(self):
        for member in self.members:
            member.PCSes.remove(self) # Remove the PCS from the member's list of PCSes they're a member of
            if self.owner == member:
                member.ownedPCSes.remove(self) # Remove the PCS from the owner's list of PCSes they're owner of
        self.alive = False
        self.owner.organisation.pcsCounter -= 1

    def log(self):
        return [len(self.members), self.lifetime]

    def step(self):
        # Update the combined knowledge and competence
        self.assembledKnowledge = union([m.knowledge for m in self.members]) # Take the union of the PCS members' own knowledge graphs

        # TODO The innovativeness is now averaged. This could be adjusted, e.g. to be the min/max/mode/etc.
        innovativeness_list = list(map(lambda m: m.innovativeness, self.members))
        self.combinedInnovativeness = sum(innovativeness_list)/len(innovativeness_list)

        # TODO The prediction is now averaged. This could be adjusted, e.g. to be the min/max/mode/etc.
        pc_list = list(map(lambda m: m.predictionCompetence, self.members))
        self.combinedPredictionCompetence = sum(pc_list)/len(pc_list)

    def checkAge(self):
        # Check how long the PCS has operated and whether it should still exist in this timestep, or whether it should disband
        if self.owner.model.t - self.dateOfBirth >= self.lifetime:
            self.disband()