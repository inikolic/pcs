# pytest tests
import pytest

import sys
from pathlib import Path
import agentpy as ap

# Add the parent directory to the Python path
sys.path.append(str(Path(__file__).resolve().parent.parent))

# Now you can import PCSModel from model.py
from model import PCSModel
from parameters import parameters


class TestFitness:
    # setup model
    def setup(self):
        # sample = ap.Sample(parameters, randomize=False)
        self.model = PCSModel(parameters)
        self.model.setup()

    def test_fitness_layers_between_0_and_1(self):
        # Test that fitness of layers is between 0 and 1 on each node
        for layer in self.model.landscape.layers:
            for node_number in layer.nodes():
                fitness = layer.nodes[node_number]['fitness']
                assert 0 <= fitness <= 1

    def test_fitness_retrieval_layers(self):
        # Test that fitness is retrieved correctly for layers
        for i, layer in enumerate(self.model.landscape.layers):
            for node_number in layer.nodes():
                fitness_retrieved = self.model.landscape.getFitnessForLayer(i, node_number)
                fitness = layer.nodes[node_number]['fitness']
                assert fitness_retrieved == fitness

    def test_estimated_fitness_between_0_and_1(self):
        # Test if the initial estimated fitness is between 0 and 1 or unknown, 5 steps, each step
        for _ in range(5):
            self.model.run(steps=1, seed=0, display=False)
            for ceo in self.model.CEOs:
                for manager in ceo.managers:
                    for employee in manager.employees:
                        for location in employee.memory.locations:
                            for fitness in location.estimatedFitness:
                                # Estimated fitness is either between 0 and 1 or unknown
                                if fitness != "unknown":
                                    assert 0 <= fitness <= 1

    def test_fitness_organisation(self):
        # Test if the fitness of an organisation is between 0 and 1
        for _ in range(5):
            self.model.run(steps=1, seed=0, display=False)
            for organisation in self.model.organisations:
                for absolute_fitness in organisation.absoluteFitnessHistory:
                    assert 0 <= absolute_fitness <= 1
                for fitness in organisation.fitnessHistory:
                    assert 0 <= fitness <= 1

    def test_organizational_weights_sum_to_1(self):
        # Test if the weights of an organisation sum to 1
        for _ in range(5):
            self.model.run(steps=1, seed=0, display=False)
            for organisation in self.model.organisations:
                sum_weights = 0
                for weight in organisation.beliefSystem.weights:
                    sum_weights += weight
                assert sum_weights == pytest.approx(1.0)
