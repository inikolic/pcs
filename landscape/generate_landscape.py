import sys
import numpy as np
import itertools
import random
import networkx as nx

def imatrix_rand(N, K): # generate random interaction matrix
    Int_matrix_rand = np.zeros((N, N))
    for aa1 in np.arange(N):
        Indexes_1 = list(range(N))
        Indexes_1.remove(aa1)  # remove self
        np.random.shuffle(Indexes_1)
        Indexes_1.append(aa1)
        Chosen_ones = Indexes_1[-(K+1):]  # this takes the last K+1 indexes
        for aa2 in Chosen_ones:
            Int_matrix_rand[aa1, aa2] = 1  # we turn on the interactions with K other variables
    return(Int_matrix_rand)


def calc_fit(N, K, NK_land_, inter_m, Current_position, Power_key_):
    '''
    Takes the landscape and a given combination and returns a vector of fitness
    values for the vector of the N decision variables.
    '''
    Fit_vector = np.zeros(N)
    for ad1 in np.arange(N):
        Fit_vector[ad1] = NK_land_[np.sum(Current_position * inter_m[ad1]
                                          * Power_key_), ad1]
    return(Fit_vector)


def comb_and_values(N, K, NK_land_, Power_key_, inter_m):
    '''
    Calculates values for all combinations on the landscape. The resulting
    array contains:
    - the first columns indexed from 0 to N-1 are for each of the combinations
    - the column indexed N is for the total fit (average of the entire vector)
    - column indexed N+1 represents the id of the location
    - column indexed N+2 is a dummy, with 1 indicating a local peak
    - the last column is a dummy, with 1 indicating the global peak
    '''
    Comb_and_value = np.zeros((2**N, N+4))  # to capture the results
    c1 = 0  # starting counter for location
    for c2 in itertools.product(range(2), repeat=N):
        # this takes time so be carefull with landscapes of bigger size
        Combination1 = np.array(c2)  # taking each combination
        fit_1 = calc_fit(N, K, NK_land_, inter_m, Combination1, Power_key_)
        Comb_and_value[c1, :N] = Combination1  # combination and values
        Comb_and_value[c1, N] = np.mean(fit_1)
        Comb_and_value[c1, N+1] = c1
        c1 = c1 + 1
    for c3 in np.arange(2**N):  # now let's see if it is a local peak
        loc_p = 1  # first, assume it is
        for c4 in np.arange(N):  # check the local neighbourhood
            new_comb = Comb_and_value[c3, :N].copy().astype(int)
            new_comb[c4] = abs(new_comb[c4] - 1)
            if ((Comb_and_value[c3, N] <
                 Comb_and_value[np.sum(new_comb*Power_key_), N])):
                loc_p = 0  # if smaller than the neighbour, then it is not peak
        Comb_and_value[c3, N+2] = loc_p
    max_ind = np.argmax(Comb_and_value[:, N])
    Comb_and_value[max_ind, N+3] = 1
    return(Comb_and_value)

def generate_nk_landscape(N, K, name, edge_removal_percentage, Power_key_):
    Int_matrix = imatrix_rand(N, K).astype(int)
    NK_land = np.random.rand(2**N, N)  # this is a table of random U(0,1) numbers
    
    # Now it is time to survey the topography of our NK landscape
    Landscape_data = comb_and_values(N, K, NK_land, Power_key_, Int_matrix)

    # saving the raw landscape matrix into a binary file
    extra_naming = name+'_' if name != "" else ""
    np.save('../data/landscape/'+extra_naming+'N'+str(N)+'K'+str(K)+'_'+str(int(edge_removal_percentage*100))+'_raw.npy', Landscape_data)

    return Landscape_data

def generate_landscape(N, K, edge_removal_percentage, name):
    Power_key = np.power(2, np.arange(N - 1, -1, -1))  # used to find addresses on the landscape

    nk_data = generate_nk_landscape(N, K, name, edge_removal_percentage, Power_key)
    G = nx.Graph()
    edgelist = []

    # creating list of edges based on neighbourhood defined over a unit Hamming distance
    for comb in np.arange(2**N):
        for neighbour in np.arange(N):  # iterate over local neighbourhood
            new_comb = nk_data[comb, :N].copy().astype(int)
            new_comb[neighbour] = abs(new_comb[neighbour] - 1)
            if (int(nk_data[np.sum(new_comb*Power_key), N+1]), int(nk_data[comb, N+1])) not in edgelist:
                edgelist.append((int(nk_data[comb, N+1]), int(nk_data[np.sum(new_comb*Power_key), N+1])))

    node_attrs = {}
    for row in nk_data:
        node_attrs[int(row[N+1])] = {"fitness": row[N]}

    G.add_edges_from(edgelist)
    nx.set_node_attributes(G, node_attrs)

    # randomly pick a percent of the edges to remove from the initial neighbourhood network
    num_edges_to_remove = int(edge_removal_percentage*(2**N)*N/2)
    edges_to_remove = random.choices(edgelist, k=num_edges_to_remove)
    G.remove_edges_from(edges_to_remove)

    extra_naming = name+'_' if name != "" else ""
    nx.write_gexf(G, '../data/landscape/'+extra_naming+'N'+str(N)+'K'+str(K)+'_'+str(int(edge_removal_percentage*100))+'.gexf')


generate_landscape(int(sys.argv[1]), int(sys.argv[2]), float(sys.argv[3]), sys.argv[4] if len(sys.argv) > 4 else "")