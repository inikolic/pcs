import networkx as nx
import matplotlib.pyplot as plt
from landscape.visualisation import MultiplexNetworkVisualisation
import os

# Get the directory of the current script
script_dir = os.path.dirname(os.path.abspath(__file__))


class MultiplexLandscape:

  def __init__(self, file_list):
    self.num_layers = len(file_list)
    self.layers = []
    for filename in file_list:
      data_file_path = os.path.join(script_dir, '..', 'data', 'landscape', filename + '.gexf')
      data_file_path = os.path.normpath(data_file_path)
      self.layers.append(nx.read_gexf(data_file_path, node_type=int))
    
    self.path_length = [dict(nx.all_pairs_shortest_path_length(layer)) for layer in self.layers]

  def getFitnessForLayer(self, layer, node):
    nodes = self.layers[layer].nodes(data=True)
    fitness = nodes[node]["fitness"]
    return fitness

  def getLandscapeFitness(self, node):
    fitness_list = []
    for l_idx in range(self.num_layers):
      nodes = self.layers[l_idx].nodes(data=True)
      fitness_list.append(nodes[node]["fitness"])

    return fitness_list

  def visualise(self, disableShow=True, node_size=300):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    MultiplexNetworkVisualisation(self.layers, ax=ax, layout=nx.spring_layout, node_size=node_size)
    ax.set_axis_off()
    if not disableShow:
      plt.show()
