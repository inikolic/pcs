import sys
import networkx as nx

def make_multiplex_landscape(name, graphs):
    G_mpx = nx.MultiGraph()
    G_mpx.add_nodes_from(graphs[0])
    for i, graph in enumerate(graphs):
        for k in range(0, len(graph.nodes)):
            G_mpx.nodes[k][i] = graph.nodes[k]["fitness"]
        G_mpx.add_edges_from(graph.edges, layer=i)

    nx.write_gexf(G_mpx, '../data/landscape/'+name+'.gexf')


graphs = []
for i in range(2, len(sys.argv)):
    graphs.append(nx.read_gexf('../data/landscape/'+sys.argv[i]+'.gexf', node_type=int))

make_multiplex_landscape(sys.argv[1], graphs)