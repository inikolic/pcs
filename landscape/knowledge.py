import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from landscape.visualisation import MultiplexNetworkVisualisation
from collections import deque

# Generates the knowledge union of knowledge objects passed
def union(knowledgeList):
  knowledge_union = Knowledge(knowledgeList[0])
  for knowledge in knowledgeList[1:]:
    for l_idx, layer in enumerate(knowledge.layers):
      reduced_view = nx.subgraph_view(layer, filter_node=lambda n: layer.degree[n] > 0)
      knowledge_union.layers[l_idx] = nx.compose(knowledge_union.layers[l_idx], reduced_view)
      if knowledge.isLayerActive[l_idx]:
        knowledge_union.layerSharingNum[l_idx] += 1
  
  for l_idx, layer in enumerate(knowledge_union.layers):
    if layer.number_of_edges() > 0:
      knowledge_union.isLayerActive[l_idx] = True

  return knowledge_union

# Generates the knowledge overlap of knowledge objects passed
def hasOverlap(knowledgeList):
  has_intersect = False
  for l_idx, layer in enumerate(knowledgeList[0].layers):
    intersect = set(knowledgeList[0].nodesInLayers[l_idx])
    for knowledge in knowledgeList[1:]:
      intersect = intersect.intersection(knowledge.nodesInLayers[l_idx])
    if len(intersect) > 0:
      has_intersect = True

  return has_intersect

"""
 - returns a set of edges that can be shared between two individuals (source and target) 
   in each available layer
 - one instance in the shareable_edges array is another array, containing the edge, then the 
   layer index
 - first the overlapping nodes are considered in the two knowledge graphs
 - then the neighbours of these overlapping nodes are identified that are not in the target
   knowledge graph
 - the edges are returned for the above neighbourhoods
 - in case layer reveal is allowed, connections between nodes (neighbourhood of the 
   overlapping nodes) that are in an unknown layer of the target are also returned
"""
def getShareableKnowledge(sourceKnowledge, targetKnowledge, isLayerRevealAllowed):
  shareable_edges = []
  for l_idx in range(len(sourceKnowledge.layers)):
    intersection = set(sourceKnowledge.nodesInLayers[l_idx]).intersection(targetKnowledge.nodesInLayers[l_idx])
    for node in intersection:
      for edge in sourceKnowledge.layers[l_idx].edges(node):
        edge_l = [edge, l_idx]
        if (edge_l not in shareable_edges) and not targetKnowledge.layers[l_idx].has_edge(*edge):
          shareable_edges.append(edge_l)

  return shareable_edges


class Knowledge:
  def __init__(self, *args):

    # 'Regular' initialisation of knowledge object
    if len(args) > 1:
      landscape = args[0]
      sourceNode = args[1]
      initLayers = args[2]
      initNodeNum = args[3]
      initMaxDegree = args[4]
      capacity = args[5]

      self.landscape = landscape
      self.capacity = capacity
      self.layers = []
      self.nodesInLayers = []
      self.isLayerActive = []
      self.learned_edges = deque()

      # in case of a union, this checks whether one layer is only contributed by one person or not
      # helps to check knowledge overlap
      self.layerSharingNum = []

      # generate knowledge graph as the subgraph of the landscape
      for l_idx in range(len(self.landscape.layers)):
        G = nx.create_empty_copy(self.landscape.layers[l_idx])
        self.isLayerActive.append(False)
        self.layerSharingNum.append(0)
        if l_idx in initLayers:
          self.isLayerActive[l_idx] = True
          self.layerSharingNum[l_idx] += 1
          iter_num = 1
          node_num = 1
          nodes_to_consider = [sourceNode]
          while node_num <= initNodeNum and iter_num < initNodeNum*10:
            iter_num += 1
            selected_node = np.random.choice(nodes_to_consider)
            if G.degree[selected_node] <= initMaxDegree:
              neighbours = [n for n in self.landscape.layers[l_idx].neighbors(selected_node)]
              new_node = np.random.choice(neighbours)
              if not G.has_edge(selected_node, new_node):
                G.add_edge(selected_node, new_node)
                self.learned_edges.append((selected_node, new_node,))
                if new_node not in nodes_to_consider:
                  nodes_to_consider.append(new_node)
                  node_num += 1
          
        self.layers.append(G)
        reduced_view = nx.subgraph_view(G, filter_node=lambda n: G.degree[n] > 0)
        self.nodesInLayers.append(list(reduced_view.nodes))

    # This class is also used for facilitating the implementation of knowledge overlaps, given below
    else:
      knowledge = args[0]
      self.landscape = knowledge.landscape
      self.layers = []
      self.isLayerActive = knowledge.isLayerActive
      self.layerSharingNum = knowledge.layerSharingNum
      self.nodesInLayers = knowledge.nodesInLayers
      self.capacity = knowledge.capacity
      self.learned_edges = knowledge.learned_edges

      for layer in knowledge.layers:
        self.layers.append(layer.copy())


  def addEdge(self, edge, layer):
    self.layers[layer].add_edge(*edge)
    self.isLayerActive[layer] = True
    self.learned_edges.append(edge)
    if self.layers[layer].number_of_edges() > self.capacity: # Forget an old edge if mind capacity exceeded
      self.layers[layer].remove_edge(*self.learned_edges.popleft())
    reduced_view = nx.subgraph_view(self.layers[layer], filter_node=lambda n: self.layers[layer].degree[n] > 0)
    self.nodesInLayers[layer] = reduced_view.nodes

# Calculate distance (number of edges) between two edges
  def distance(self, sourceNode, targetNode):
    flattened_multiplex_graph = self.layers[0].copy()
    for layer in self.layers[1:]:
      flattened_multiplex_graph.update(edges=layer.edges)

    return nx.shortest_path_length(flattened_multiplex_graph, sourceNode, targetNode)

  def unknownNeighbors(self, sourceNode, layer):
    all_neighbours = [n for n in self.landscape.layers[layer].neighbors(sourceNode)]
    known_neighbours = [n for n in self.layers[layer].neighbors(sourceNode)]
    unknown_neighbours = []
    for node in all_neighbours:
      if node in known_neighbours:
        unknown_neighbours.append(node)

    return unknown_neighbours

  def visualise(self, includeUnreachable=False, disableShow=False, nodeSize=80):
    reduced_layers = []
    for l_idx, layer in enumerate(self.layers):
      if self.isLayerActive[l_idx]:
        reduced_layers.append(layer.copy())

    if not includeUnreachable:
      for node in self.layers[0].nodes():
        remove_node = True
        for layer in self.layers:
          if layer.degree[node] > 0:
            remove_node = False

        if remove_node:    
          for layer in reduced_layers:
            layer.remove_node(node)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    MultiplexNetworkVisualisation(reduced_layers, ax=ax, layout=nx.spring_layout, node_size=nodeSize)
    ax.set_axis_off()
    if not disableShow:
      plt.show()