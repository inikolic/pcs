import matplotlib.pyplot as plt
from landscape.multiplex import MultiplexLandscape
from landscape.knowledge import Knowledge, union

# load a multiplex landscape
landscape = MultiplexLandscape(('N9K1_10', 'N9K1_20', 'N9K1_30'))

kn1 = Knowledge(
    landscape, # landscape to generate the knowledge graph from
    5, # source node 
    [0, 1], 
    10, # number of nodes in the knowledge graph
    3, # average node degree upon graph generation
    100, # mind capacity in terms of edges (not relevant here)
)

kn2 = Knowledge(
    landscape, # landscape to generate the knowledge graph from
    15, # source node 
    [1, 2], 
    10, # number of nodes in the knowledge graph
    3, # average node degree upon graph generation
    100, # mind capacity in terms of edges (not relevant here)
)

# GRAPH VISUALISATION EXPLANATION:
# - colour of nodes represent the fitness value there, a deeper shade depicting
#   a larger fitness value
# - the dotted lines represent the multiplexity: connecting the same nodes over
#   the different landscapes

# visualise the landscape
landscape.visualise()

# visualise knowledge graphs
kn1.visualise(disableShow=True)
kn2.visualise(disableShow=True)

# create the union of the two knowledge graphs
# same function is being called when individuals work together via a PCS
kn_union = union([kn1, kn2])

# visualise the knowledge union
kn_union.visualise()

# drawing the figures
plt.show()