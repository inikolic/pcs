***CONTrOL*** *is an agent-based model developed for the purpose of studying behavioral mechanisms influencing social learning and performance within and across business organisations. CONTrOL stands for Complex Organisational and Network-driven Transmissions resulting in Organisation Learning.*

# Visualise module

```{eval-rst}
.. automodule:: visualise
   :members:
   :undoc-members:
   :show-inheritance:
```

The landscapes and generated knowledge graphs can be visualised by adjusting and running the `visualise.py` script in the root folder.
The available (already generated) landscapes are in the `data/landscape` folder. 

**Note: this module runs independently from the [main module](#main.py)**

## Library and module imports

Pyplot is imported which provides a state-based interface to matplotlib.
Also, several class objects are imported from the Landscape package.
```python
import matplotlib.pyplot as plt
from landscape.multiplex import MultiplexLandscape
from landscape.knowledge import Knowledge, union
```

## Multiplex knowledge landscape generation

The following code provides an example of the `landscape` generation by calling the `MultiplexLandscape()` from the [`landscape.multiplex` module](#l.multiplex) with three pregenerated layers. It then takes the `landscape` as an argument to create a knowledge graph using the `Knowledge` function from the [`landscape.knowledge` module](#l.knowledge).

```python
landscape = MultiplexLandscape(('N9K1_10', 'N9K1_20', 'N9K1_30'))

kn1 = Knowledge(
    landscape,
    5,
    [0, 1], 
    10,
    3,
    100,
)

kn2 = Knowledge(
    landscape, 
    15,
    [1, 2], 
    10,
    3,
    100,
)
```

## Knowledge graph visualisation

The `visualise()` function is used to visualise the landscape. The colour of the nodes represent the fitness value there, with a deeper shade depicting a larger fitness value. The dotted lines represent the multiplexity of the knowledge graph and visualise how the same nodes are connected over the different landscapes.
This works similarly for a union of two or more knowledge graphs, for example when several employees are working together in a PCS.

```python
landscape.visualise()

kn1.visualise(disableShow=True)
kn2.visualise(disableShow=True)

kn_union = union([kn1, kn2])

kn_union.visualise()

plt.show()
```


