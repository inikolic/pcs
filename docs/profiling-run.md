***CONTrOL*** *is an agent-based model developed for the purpose of studying behavioral mechanisms influencing social learning and performance within and across business organisations. CONTrOL stands for Complex Organisational and Network-driven Transmissions resulting in Organisation Learning.*

# Profiling-run module

The `cProfile.run()` function is part of the `cProfile` module in Python, which is used for profiling and analysing the performance of Python programs. Profiling involves measuring the execution time and other statistics of different parts of your code to identify performance bottlenecks and areas that can be optimized. `cProfile` is particularly useful for identifying which functions or code segments consume the most time during program execution.

```{eval-rst}
.. automodule:: profiling-run
   :members:
   :undoc-members:
   :show-inheritance:
```
