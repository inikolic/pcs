***CONTrOL*** *is an agent-based model developed for the purpose of studying behavioral mechanisms influencing social learning and performance within and across business organisations. CONTrOL stands for Complex Organisational and Network-driven Transmissions resulting in Organisation Learning.*

(main.py)=
# Main module

```{eval-rst}
.. automodule:: main
   :members:
   :undoc-members:
   :show-inheritance:
```

## Parameter settings

The main.py module contains a `parameters` dictionary of models parameter names, values, data type, and possible or logical value ranges.

## Setting up and running the ABM simulations

The following code sets up and runs an Agent-Based Model (ABM) simulation using several AgentPy (`ap.`) methods.
* It creates an instance of `ap.Sample` by passing `parameters` to it. The `randomize=False` argument indicates that the sample should not be randomized.
* It creates an `ap.Experiment` by specifying the model class `PCSModel`, the `sample`, the number of `iterations` (3), and records the data during the experiment for data analysis purposes.
* It runs the experiment, potentially using multiple CPU cores (`n_jobs=-1`), and sets the verbosity level which controls the amount of information that is displayed to the user or written to logs during the execution of the experiment. The results of the experiment are stored in the `results` variable.
* It defines the experiment name (`exp_name`) as "final."
* It saves the experiment results, associating them with the name "final."
```python
sample = ap.Sample(parameters, randomize=False)

exp = ap.Experiment(PCSModel, sample, iterations=3, record=True)
results = exp.run(n_jobs=-1, verbose=10)

exp_name = "final"
results.save(exp_name=exp_name)
```

