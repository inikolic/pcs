% PCS Documentation documentation master file, created by
% sphinx-quickstart on Wed Sep 27 12:30:32 2023.
% You can adapt this file completely to your liking, but it should at least
% contain the root `toctree` directive.

# Welcome to CONTrOL's code documentation

***CONTrOL*** *is an agent-based model developed for the purpose of studying behavioral mechanisms influencing social learning and performance within and across business organisations. CONTrOL stands for Complex Organisational and Network-driven Transmissions resulting in Organisation Learning.*

```{toctree}
:caption: 'Contents:'
:maxdepth: 2

modules
```

# Indices and tables

- {ref}`genindex`
- {ref}`modindex`
- {ref}`search`
