# CONTrOL

***CONTrOL*** *is an agent-based model developed for the purpose of studying behavioral mechanisms influencing social learning and performance within and across business organisations. CONTrOL stands for Complex Organisational and Network-driven Transmissions resulting in Organisation Learning.*

```{toctree}
:maxdepth: 5

organisation
control
landscape
model
profiling-run
visualise
main
```
