.. PCS Documentation documentation master file, created by
   sphinx-quickstart on Wed Sep 27 12:30:32 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PCS Documentation's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
