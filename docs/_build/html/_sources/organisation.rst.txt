organisation package
====================

Submodules
----------

organisation.CEO module
-----------------------

.. automodule:: organisation.CEO
   :members:
   :undoc-members:
   :show-inheritance:

organisation.Department module
------------------------------

.. automodule:: organisation.Department
   :members:
   :undoc-members:
   :show-inheritance:

organisation.Employee module
----------------------------

.. automodule:: organisation.Employee
   :members:
   :undoc-members:
   :show-inheritance:

organisation.Individual module
------------------------------

.. automodule:: organisation.Individual
   :members:
   :undoc-members:
   :show-inheritance:

organisation.Manager module
---------------------------

.. automodule:: organisation.Manager
   :members:
   :undoc-members:
   :show-inheritance:

organisation.Organisation module
--------------------------------

.. automodule:: organisation.Organisation
   :members:
   :undoc-members:
   :show-inheritance:

organisation.PCS module
-----------------------

.. automodule:: organisation.PCS
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: organisation
   :members:
   :undoc-members:
   :show-inheritance:
