import datetime
import cProfile
import agentpy as ap
from model import PCSModel

def modelRunToProfile():

    parameters = {
        'seed': 0,
        'steps': 120,

        # organisations
        'numCEOs': 8,
        'numManagersPerOrg': 4,
        'numEmployeesPerDept': 6,
        'reimbursementWindow': 4, # monthly by default
        'maxCompetitorsToConsider': 2,
        'orgDepInitialDistance': 15,
        'depEmployeeInitialDistance': 5,
        'resourceMargin': 10, # int; x >= 1
        'maxBonus': 5,
        'maxBonusMultiplier': 1,

        # landscapes
        'landscape': ap.Values(
            ('N9K1_10', 'N9K1_20', 'N9K1_30'), # smooth
            ('N9K5_10', 'N9K5_20', 'N9K5_30'), # rugged
            ('N9K1_10', 'N9K5_20', 'N9K1_30') # mixed
        ), # which multiplex landscape combinations to use (from the data/landscape folder)
        'energyPerMove': 1, # int; x >= 1
        'ERIThreshold': 1.0, # float; 0 <= x <= 1

        # Initial individual knowledge graph parameters
        'initKnowledgeLayerNum': 2,
        'initKnowledgeNodeNum': 10,
        'initKnowledgeGraphDegree': 3,
        'mindCapacity': 150,

        # Personal competency
        'initTotalEnergy': 10,
        'modeInnovativeness': 0.5, # float; 0 <= x <= 1
        'modePredictionCompetence': 0.5, # float; 0 <= x <= 1
        'modeEvaluationCompetency': 0.5, # float; 0 <= x <= 1
        'competencyLearningStep': 0.05,
        'minMemory': 4, # int; x >= 1; x < maxMemory
        'maxMemory': 6, # int; x >= 1; x > minMemory
        'maxMaxNumFriends': 10, # int; x >= 1
        'learningCost': 1, # int; X >= 1; 1 iteration of learning costs X energy
        'socialisingWindow': 2, # ticks, every two weeks by default

        'proposalAveragingWindow': 4, # int

        # MCS
        'mcsEvaluationWindow': 24,  # every half a year
        'dcsEvaluationWindow': 4, # monthly by default
        'mcsGradientStep': 0.1,
        'maxResourceMargin': 0.3,
        'mcs': ap.Values(
            True, 
            False,
        ), # bool; whether DCSes are enabled this run
        'pcs': ap.Values(
            True, 
            False
        ), # bool; whether PCSes are enabled this run

        # PCSes
        'maxPCSSize': 5, # int; x >= 3 number of employees (lower bound in PCS class setup is 2)
        'maxLifetime': 4, # int; x >= 1 ticks that the PCS lives

        # Parameters for code optimisation
        'learningAttempts': 2
    }

    sample = ap.Sample(parameters, randomize=False)

    exp = ap.Experiment(PCSModel, sample, iterations=1, record=True)
    results = exp.run()

    exp_name = "profiler"
    results.save(exp_name=exp_name)

cProfile.run('modelRunToProfile()')