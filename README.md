# Participatory Control Systems: An Agent-based Model

## Setup

The project requires Python to be installed. After making sure one has it installed, open a terminal in the project root folder and create a virtual environment by running the following command:

```
python -m venv /menv
```

Or in case of an Anaconda environment:

```
conda create --name menv
```

Subsequently, install all the library dependencies by running the following command:

```
pip install -r requirements.txt
```

## Running experiment

Before running any of the scripts in the project, one always has to enter the virtual environment. Run this command in order to do so (still in the root folder of th):

```
.\menv\Scripts\activate
```

Or in case of an Anaconda environment:

```
conda activate menv
```

The experiment can be started by running the `main.py` script:

```
python main.py
```

## Changing experiment

The experiment parameters can be adjusted by editing the `parameters` dictionary in the `main.py`. Scenarios can be set up by defining value ranges for a given set of parameters. Following this, the simulation engine will create all possible combinations of these parameters for the given subset, and will run an experiment for each of the scenarios. The number of repetitions of each scenario run can be increased by changing the `iterations` parameter of the `ap.Experiment()` function. To learn how to define such ranges, please consult [Multi-run experiments](https://agentpy.readthedocs.io/en/latest/overview.html#multi-run-experiments) and [Parameter samples reference](https://agentpy.readthedocs.io/en/latest/reference_sample.html)

In the landscape parameter, a combination of landscape layers can be specified to define the multiplex landscape. Please note, that only landscapes with the same N number can be combined. The available (already generated) landscapes are in the `data/landscape` folder. In order to generate new landscape layers, consult the 'Generating landscapes' section.

## Data analysis

Data analysis can be conducted in Jupyter notebook, for which, run the following command in the project root folder (after entering the virtual environment):

```
jupyter notebook
```

After Jupyter notebook opens in the browser, select the `Data analysis` notebook. Replace `"final"` with the conducted experiment name in the first line of the second cell (if the experiment name has been changed).

## Generating landscapes

To generate new landscape layers, first enter the landscape folder by running:

```
cd landscape
```

Then run the following script:


```
python generate_landscape.py <N> <K> <edge-removal-percentage>
```

where `<N>`, `<K>` and `<edge-removal-percentage>` need to be replaced with the respective desired values, such as `python generate_landscape.py 9 1 0.3`.

## Landscape visualisation

The landscapes and generated knowledge graphs can be visualised by adjusting and running the `visualise.py` script in the root folder:

```
python visualise.py
```

## NK_model license

For NK fitness landscape creation, the NK_model developed by Workiewicz (2020, January 13, https://github.com/Mac13kW/NK_model) has been used. This model is licensed under a MIT license which states the following:

```
The MIT License (MIT)

Copyright (c) 2014 Maciej Workiewicz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

