import datetime

import agentpy as ap
from model import PCSModel

# Below are the model parameter names, values, data type, and possible or logical value ranges
parameters = {
    'seed': 0, # int; used to 'seed' the Pseudo Random Number Generator, see https://agentpy.readthedocs.io/en/latest/guide_random.html
    'steps': 50, # int; 1 tick = 1 week - run 5 years in total: 52*5=260

    # organisations
    'numCEOs': 3, # int; x >= 1
    'numManagersPerOrg': 3, # int; x >= 1
    'numEmployeesPerDept': 5, # int; x >= 1
    'reimbursementWindow': 4, # int; x >= 1; managers reimburse employees once every X steps; monthly (once per 4 weeks) by default
    'maxCompetitorsToConsider': 2,# int; x >= 1. If you want to disable benchmarking, set mcs to False (note: this disables more than just benchmarking)
    'orgDepInitialDistance': 15, # int; x >= 1
    'depEmployeeInitialDistance': 5, # int; x >= 1
    'resourceMargin': 10, # int; x >= 1
    'maxBonus': 5, # int; x >= 1
    'maxBonusMultiplier': 1, # float; x >= 1
    'proposalAveragingWindow': 4, # int; x >= 1


    # landscapes
    'landscape': ap.Values(
        ('N9K1_10', 'N9K1_20', 'N9K1_30'), # Smooth
        ('N9K5_10', 'N9K5_20', 'N9K5_30'), # Rugged
        ('N9K1_10', 'N9K5_20', 'N9K1_30') # Mixed
    ), # which multiplex landscape combinations to use (from the data/landscape folder)


    # Initial individual knowledge graph parameters
    'initKnowledgeLayerNum': 2, # int; x >= 1
    'initKnowledgeNodeNum': 10, # int; x >= 1
    'initKnowledgeGraphDegree': 3, # int; x >= 1
    'mindCapacity': 150, # int; x >= 1

    # Personal characteristics of employees
    #?'ERIThreshold': 9.0, # float; 0 <= x <= 1 #FALSE STATEMENT, KAN OOK >1 ZIJN. #TODO VERVANGEN VOOR ABSOLUTE BONUS THRESHOLD
    'bonusThreshold': 0.25, # float; 0 <= x <= 10
    'modeInnovativeness': 0.5, # float; 0 <= x <= 1
    'modePredictionCompetence': 0.5, # float; 0 <= x <= 1
    'competencyLearningStep': 0.05, # float; 0 < x < 1
    'minMemory': 4, # int; 1 <= x < maxMemory
    'maxMemory': 6, # int; x > minMemory
    'maxMaxNumFriends': 10, # int; x >= 1
    'learningCost': 1, # int; X >= 1; 1 iteration of learning costs X energy

    # MCS
    'mcsEvaluationWindow': 24, # int; x >= 1; evaluate MCS once every X steps; by default every half a year (rounded down to 24 to align with DCS evaluation)
    'dcsEvaluationWindow': 4, # int; x >= 1; evaluate DCS once every X steps; monthly (once per 4 weeks) by default
    'mcsGradientStep': 0.1, # float; 0 < x < 1
    'maxResourceMargin': 0.3, # float; 0 < x < 1
    'mcs': ap.Values( # bool; whether DCSes are enabled this run
        True, 
        False,
    ),

    # PCSes
    'pcs': ap.Values(  # bool; whether PCSes are enabled this run
        True,
        False
    ),
    'maxPCSSize': 6, # int; x >= 3 number of employees (lower bound in PCS class setup is 2)
    'maxLifetime': 10, # int; x >= 1 ticks that the PCS lives

    # Parameters for code optimisation
    'learningAttempts': 2 # int; x > 1: how many learning attempts an employee will undertake at most in one step
}

sample = ap.Sample(parameters, randomize=False)

exp = ap.Experiment(PCSModel, sample, iterations=2, record=True)
results = exp.run(n_jobs=-1, verbose=10)

exp_name = "test_pcs3"
results.save(exp_name=exp_name)